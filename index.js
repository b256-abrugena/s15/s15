// console.log("Hello World")

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let statementOne = "First Name: ";
	let firstName = "David Polo";

	let statementOneFinal = statementOne + firstName;
	console.log(statementOneFinal);

	let statementTwo = "Last Name: ";
	let lastName = "Abrugena";

	let statementTwoFinal = statementTwo + lastName;
	console.log(statementTwoFinal);

	let statementThree = "Age: ";
	let age = 29;

	let statementThreeFinal = statementThree + age;
	console.log(statementThreeFinal);

	let statementFour = "Hobbies:"
	console.log(statementFour);

	let myHobbies = ["Watching Kdrama", "Solving Math Equations", "Travelling"];
	console.log(myHobbies);

	let statementFive = "Work Address:"
	console.log(statementFive);

	let myAddress = {
		houseNumber: "Blk 1 Lot 19",
		street: "Ciudad Grande",
		city: "Santa Rosa",
		state: "Laguna"
	}
	console.log(myAddress);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	let firstStatement = 'My full name is ' + fullName;
	console.log(firstStatement);

	let currentAge = 40;
	let secondStatement = "My current age is: " + currentAge;
	console.log(secondStatement);
	
	let thirdStatement = "My Friends are:"
	console.log(thirdStatement);

	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);

	let fourthStatement = "My Full Profile:"
	console.log(fourthStatement);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log(profile);

	let fifthStatement = "My bestfriend is: ";
	let bestFriend = "Bucky Barnes";
	let sixthStatement = fifthStatement + bestFriend;
	console.log(sixthStatement);

	let seventhStatement = "I was found frozen in: ";
	let locationAt = "Arctic Ocean";
	let eighthStatement = seventhStatement + locationAt;
	console.log(eighthStatement);